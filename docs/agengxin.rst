更新记录
========

.. contents::
产品更新记录
------------

-  | 各位行云人：
   | 大家好，中台V5.9.0已于2021.07.19晚上上线，更新内容如下：

-  | 在使用系统时，如有任何需求和建议欢迎在钉钉中搜索 fanwei 或 sq
     一键发起和产品经理的对话进行反馈。
   | 中台产品研发团队

-  【订单中心】API拉单(中台对接第三方)-文案及提示优化

-  

-  2021.07.19 V5.9上线

   1.  【权限中心-部门与员工列表】增加更新日志，记录异常组织或部门下关联的渠道数据

   2.  【权限中心-部门与员工列表】增加更新日志，记录组织、部门、员工数据更新记录

   3.  【权限中心-部门与员工列表】删除【锁定】操作及锁定状态，原锁定状态数据更新为停用状态

   4.  【权限中心-部门与员工列表】增加员工来源与员工状态

   5.  【权限中心-部门与员工列表】取消组织与部门的新增、编辑、删除操作

   6.  【权限中心-部门与员工列表】组织与部门增加禁用标识

   7.  【权限中心-部门与员工列表】组织架构与员工对接OA系统，同步组织、部门、姓名、工号、职务、手机号、性别、邮箱数据

   8.  【供应商管理】审核供应商_编辑供应商增加操作人日志

   9.  【渠道管理】校验渠道名称唯一性

   10. 【商品中心】条码库批量导入建档时，需将条码去掉头尾再模糊匹配，若能匹配到系统已有重复SKU则不允许建档

   11. 【商品中心】条码库批量导入SKU的模板，需增加业务负责人字段

   12. 【商品中心】集团商品库前端独立入口

   13. 【采购中心】自营批次-供货 显示重量及支持选择多个渠道

   14. 【库存中心】自营库存-退货入库-支持导出多页的当前列表结果

   15. 【库存中心】库存分配-选择自营渠道时默认勾选行云联盟

   16. 【订单中心】模板导单-增加批量修改SKU编码的操作

   17. 【订单中心】API拉单-转单失败原因中，显示转单失败商品的SKU编码

   18. 【订单中心】异常单列表-回传失败-标记是否处理

   19. 【供应商后台】联营供应商入驻申请信息和资质字段完善

   20. 【供应商后台】入库商品列表-批量导入功能，仓库入库批号非必填

   21. 【供应商后台】采购单列表，“导出（按发货单模板）根据筛选结果导出

   22. 【供应商后台】采购单列表，点击"查看"打开新页面

   23. 【供应商后台】采购单列表增加收件人显示

   24. 【供应商后台】发货单-批量修改物流单号

   25. 【供应商后台】结算单明细，导出增加发送至邮箱功能

-  2021.06.30 V5.8上线

   1.  【商品中心】中台SKU列表增加标品ID搜索条件

   2.  【商品中心】中台SKU支持不填写保质期

   3.  【商品中心】仓库货号映射增加“标品ID”字段，展示中台sku编码关联的标品ID

   4.  【商品中心】仓库货号映射模块，增加接口以便BB根据标品ID号查询仓库货号

   5.  【采购中心】自营批次列表点击“查看”，打开新的标签页面

   6.  【采购中心】代发结算单-查看明细-导出，工单和售后订单类型的数据缺少sku包装规格和sku价格的数据，目前影响形式发票的开具

   7.  【采购中心】【财务中心】代发结算单-结算明细-导出
       增加“渠道订单号”、“sku编码”和“下单数量”

   8.  【采购中心】批次列表，增加批次类型的选择及批次标记

   9.  【库存中心】批次文案统一优化(涉及BBC、BB、供应商系统)

   10. 【库存中心】出库规则中取批次的类型

   11. 【库存中心】自营库存-增加 退货入库页面和功能 [售后]

   12. 【渠道工作台】商品总库的导出优化为异步导出

   13. 【渠道工作台】报价-报价记录-日常报价类型的支持显示有效期的日期时间范围

   14. 【渠道工作台】报价-报价导入，支持导入活动价

   15. 【渠道工作台】渠道设定-是否生成0元订单设定默认为否

   16. 【订单中心】API拉单
       增加字段储存下游接口中的“供货价”，需逐一平台处理

   17. 【订单中心】API拉单-渠道售后记录-订单金额取值修改

   18. 【订单中心】API拉单-获取渠道售后记录
       逻辑优化，及固定显示-拒绝或同意按钮

   19. 【订单中心】API拉单-获取渠道售后自动扭转状态
       增加处理场景：销售订单及采购订单被人工取消成功，或采购订单无法取消创建了销售订单的售后记录时，自动将渠道售后记录待退款扭转为已完成[售后]

   20. 【订单中心】模板导单 差异增加税费的处理

   21. 【订单中心】销售订单-文案及提示优化

   22. 【订单中心】订单列表页面增加“供应商类型”筛选条件

   23. 【订单中心】采购订单
       手动出库：允许发货状态为已收货及已完成的发货单手动出库

   24. 【订单中心】采购订单
       当手动触发通知上游取消时，选择的原因为：用户主动取消，则在上游返回取消成功后，自动取消销售订单

   25. 【订单中心】待重推列表 过滤已被取消的销售订单

   26. 【订单中心】渠道订单/销售订单/采购订单 增加取消操作记录

   27. 【订单中心】发起售后-退货退款售后的表单优化及捷径状态的处理
       [售后]

   28. 【订单中心】售后订单详情显示优化(包括财务中心中的售后订单详情)
       [售后]

   29. 【订单中心】售后订单列表-确认收货操作优化[售后]

   30. 【工单中心】供应商售后工单页面、供应商调整工单页面、供应商补偿工单页面
       增加“供应商类型”数据显示和筛选条件

   31. 【供应商中心】修改供应商接收账单邮箱

   32. 【供应商中心】-商品管理
       去除商品入库时商品进入草稿箱流程，去除草稿箱菜单和页面。

   33. 【供应商中心】-代发结算单-列表默认按照账期开始时间倒序排列。

   34. 【供应商中心】支付单相关字段无需在供应商后台显示。

   35. 【供应商中心】-采购订单
       批量导出需要增加一个字段，命名为“包装规格*购买数量”，值为包装规格和购买数量字段值的乘积，所在列默认位置在“购买数量”后面。

   36. 【供应商中心】售后地址-新增和编辑售后地址时增加发货仓的选择
       [售后]

   37. [兼容代推支付单] API店铺管理中增加店铺属性：是否需要代推支付单

   38. [兼容代推支付单]
       销售订单生成成功后，判断是否需要代推支付单，如需则进入申报列表排队申报

   39. [兼容代推支付单] 申报列表增加申报状态“待申报”及列表显示优化

   40. 【API管理】原：订单中心-订单管理-采购订单-API推送记录更改页面位置：接口调用记录-上游API-采购单推送

   41. 【API管理】上游API-API取消结果查询页面增加显示及取值优化；增加查询销售订单是否被取消等

   42. 【API管理】上游API-库存不足通知-增加查询采购单是否被取消，增加操作人搜索项，及其他文案优化

   43. 【API管理】上游API-物流回传调用记录

   44. 【API管理】上游API-出库通知调用记录

   45. 【API管理】订单中心-渠道订单-API导单失败记录更改页面位置：下游API-开放平台导单失败记录

   46. 【API管理】内部API-扣减采购库存异常记录

   47. 【控制面板】物流公司模块，需要提供查询接口给BB系统定时获取最新的物流公司信息

   48. 【权限中心】增加招商系统的权限管理模块

   49. 【权限中心】创建员工时手机号新增3个国家/地区 区号

-  2021.06.08 V5.7上线

   1.  【控制面板】公司主体详情页，增加4个字段：企业类型、注册资本、营业期限、营业执照

   2.  【控制面版】增加异常地址库列表，支持添加/启用及禁用异常地址库

   3.  【控制面板】增加发货单的逾期设定

   4.  【条码库】运营系统创建商品时，需将SKU编码也同步通知给集团商品库

   5.  【商品中心】中台SKU编码生成规则改为：一级类目首字母（英文2位）+创建年月日（数字6位）+自增码（数字4位）+贸易类型（英文1位）

   6.  【商品中心】运营系统删除SKU时，若未关联标品，则需同时从集团商品库的“待处理”页面删除

   7.  【采购中心】批次列表/批次采购/批次冻结列表搜索优化及新增列表字段

   8.  【采购中心】自营管理-自营批次列表tab项 [供货中] 数据拆分为
       [部分供货] 和 [已供货]

   9.  【采购中心】自营批次-详情增加生产批次号显示

   10. 【库存中心】搜索项及列表项字段排序和字段优化

   11. 【库存中心】库存锁定页面-锁定库存方式优化及保存返回列表筛选条件不变

   12. 【渠道工作台】渠道设定-增加默认出库规则设定

   13. 【订单中心】API拉单-待确认状态下 支持关闭和取消

   14. 【订单中心】API拉单-订单详情增加发货信息的展示

   15. 【订单中心】API拉单-订单列表中增加是否已回传物流单号及增加筛选项

   16. 【订单中心】API拉单-支持异常地址关联地址库

   17. 【订单中心】订单生成-生成失败类型拆分

   18. 【订单中心】订单详情增加清关及出库及签收相关信息显示

   19. 【订单中心】【采购订单】已发货采购订单详情增加手动出库功能

   20. 【订单中心】【采购订单】采购单详情-发货信息增加清关出库状态等相关信息显示

   21. 【订单中心】【采购订单】采购单列表增加清关及出库相关信息显示

   22. 【订单中心】【发货单】发货单详情增加清关及出库及签收相关信息显示

   23. 【订单中心】【发货单】发货单列表增加清关及出库及签收相关信息显示

   24. 【订单中心】销售订单列表-增加采购单号数据列和订单备注数据列以及备注入口
       标记及列表显示样式优化

   25. 【订单中心】销售订单列表-增加已发货状态

   26. 【订单中心】售后订单-非已完成/已拒绝/已撤销状态的支持撤销

   27. 【订单中心】售后订单列表-状态名文案及增加数量提示和增加供应商类型、按采购单号、退货运单号筛选项

   28. 【订单中心】采购订单列表-增加按批次号筛选项

   29. 【订单中心】采购订单详情，增加显示批次对应的生产批次号

   30. 【订单中心】采购订单-增加已发货状态

   31. 【订单中心】发货单详情-增加发货单轨迹

   32. 【通知管理】增加售后完成通知模板-售后订单审核完成后(包括通过或拒绝)提醒至创建人

   33. 【API管理】-上游API-增加清关状态同步开关

   34. 【沙箱平台】-保税清关状态同步接口（仓库→行云）

   35. 【供应商平台】采购订单列表：增加显示[销售单号]字段

   36. 【供应商平台】采购订单列表：在采购订单页面，增加[贸易类型]字段显示和筛选条件

   37. 【供应商平台】售后订单列表-待处理和待收货增加数量标识以及操作文案优化

-  2021.05.15 V5.6上线

   -  【商品中心】商品品牌、商品分类，去掉“关联SKU数”字段，删除品牌、类目时的校验规则修改

   -  【采购中心】自营批次-列表页搜索栏增加按照业务人员的筛选条件

   -  【采购中心】自营批次-供货时可以复用已经添加的商品，对变量进行编辑

   -  【采购中心】自营批次-供货时可以直接跳转至库存分配页面

   -  【采购中心】供应商结算-代发结算单导出字段增加

   -  【库存中心】优化增加库存分配记录

   -  【库存中心】库存分配-支持批量分配

   -  【渠道工作台】销售列表与渠道SKU总库列表[可售库存]总计增加部门内共享库存

   -  【渠道工作台】渠道设定-支持设定寄售批次或非寄售批次是否自动上架

   -  【渠道工作台】渠道设定-是否允许拉取终端支付金额为0的订单

   -  【订单中心】API拉单-订单生成失败，新增批量修改SKU编码功能

   -  【订单中心】API拉单-渠道售后记录-增加渠道搜索条件

   -  【订单中心】模板导单-增加实际业务发生的时间字段

   -  【订单中心】渠道订单-订单详情订单轨迹增加管理员信息(手动操作取消和关闭时)

   -  【订单中心】寄售标记的店铺生成销售订单时，校验是否寄售批次，非寄售批次生成失败

   -  【订单中心】API拉单-渠道售后列表-根据中台自动取消情况接口反馈下游同意or拒绝

   -  【订单中心】API拉单-渠道售后记录-根据供应商返回的采购单取消结果，取消采购订单

   -  【订单中心】API-渠道售后记录-已生成采购单的自动触发通知上游取消订单（若上游已对接API取消接口）

   -  【订单中心】销售订单-搜索栏增加手机号的检索条件

   -  【订单中心】销售订单-订单详情中的模块内容支持折叠交互

   -  【订单中心】售后订单列表搜索增加发起人的检索条件

   -  【订单中心】售后单详情显示审核备注原因(包括拒绝和同意)

   -  【订单中心】发货单列表支持发货单单号搜索

   -  【API管理】店铺增加回传物流单号的时间设定

   -  【权限中心】新增库存分配的权限的设定

   -  【控制面板】新增/编辑公司主体信息时，字段校验规则修改

   -  【控制面板】公司主体详情页，增加操作日志功能

   -  【控制面板】公司主体列表页，“公司简称”文案改为“中文名称或简称

   -  【供应商管理】供应商类型增加：O2O

   -  【供应商平台】供货页面初始值优化

   -  【供应商平台】商品供货-商品单位重量复用商品中心的商品重量

   -  【供应商平台】入库商品列表-支持批量导入批次

   -  【供应商平台】导入发货单支持部分成功(失败可下载)

   -  【供应商平台】代发结算单导出增加字段；详情导出及列表导出使用统一接口保证导出字段及数据一致

-  2021.04.27 V5.5上线

   -  【商品中心】【条码库】条码库-标准商品库去掉SPU的概念，页面仅展示SKU信息，类目、品牌变为SKU属性

   -  【商品中心】【条码库】条码库-增加“批量修改类目”功能

   -  【商品中心】【条码库】条码库-增加“批量修改品牌“功能

   -  【商品中心】【条码库】条码库-增加新建单个商品的功能

   -  【商品中心】【条码库】修改导入导出功能
      (批量导出的表格去掉“SPU编码”、“SPU名称”字段及其他相关逻辑)

   -  【商品中心】【条码库】条码并联-新建/编辑条码关联的校验规则修改

   -  【商品中心】【条码库】新增“标品映射”模块，显示标品和和业务系统的商品关系

   -  【商品中心】【条码库】新增“SKU更新通知”模块，在与和业务系统信息同步通知失败时可操作重试

   -  【商品中心】SPU列表/SKU列表，原产地、类目、品牌记录为SKU属性(SPU中去除原产地字段)

   -  【商品中心】SPU列表/SKU列表，SKU属性新增“SKU规格”字段（必填），与SKU标签进行区分

   -  【商品中心】SPU列表/SKU列表，SKU编码需要由条码库统一生成并赋值。若条码库没有返回信息，则中台SKU也提示创建失败

   -  【商品中心】SPU列表/SKU列表，允许用户创建SKU时手动输入条码，不选择任何一个联想词。提交时若校验到条码在标准商品库不存在，则自动创建为条码库的非标准商品，显示在“待处理”列表页

   -  【商品中心】组合商品下SKU种类数上限由5个增加到10个

   -  【渠道工作台】商品-批次列表展示逻辑中增加部门锁定共享库存的处理

   -  【开放平台】获取商品列表/获取商品详情接口，字段“原产地”取值修改

-  2021.04.21 V5.4.2上线

   -  【库存中心】库存分配时，解除“必须给货仓分配锁定库存”的限制及提示

   -  【库存中心】库存分配页中，已关闭的渠道显示已关闭的提示

   -  【库存中心】渠道的锁定库存支持部门内的其他渠道使用（需开启
      部门内共享库存）

   -  【库存中心】库存分配页中，按所属部门选择下的部门信息中显示
      部门内共享库存 数量

   -  【渠道工作台】已上架批次与待上架批次列表新增统计字段
      部门内共享库存，并对渠道剩余库存统计更新

   -  【订单中心】商品库存消耗逻辑调整，顺序为渠道自身锁定库存>部门内渠道锁定且开启部门内共享库存>批次共享库存

-  2021.04.13 V5.4.1上线

   -  {供应商平台}【采购订单列表】增加批量取消发货

   -  {供应商平台}【采购订单列表】按发货单模板中的sku数量预置准确数量

   -  {供应商平台}【采购订单列表】、【发货单列表】、【发货逾期订单列表】增发货倒计时字段

   -  {供应商平台}【发货单列表】列表搜索和显示，增加快递公司名称、物流单号、清关单号

   -  {供应商平台}【采购订单列表】【发货单列表】搜索栏增加物流公司、物流单号、清关单号

   -  {供应商平台}【发货单列表】发货单详情，增加支付单号、清关单号

   -  【供应商售后工单】搜索栏增加供应商和申请人的搜索项

   -  【财务中心】【代发供应商结算单】列表及导出增加备注

   -  【财务中心】【代发供应商结算单】"待结算状态"列表，增加"结算单创建日期"的搜索和显示和排序

   -  【财务中心】【代发供应商结算单】点击查看和审核，打开新页面、以及列表筛选项优化

   -  【财务中心】【代发供应商结算单】供应商已结算列表，增加推送状态筛选和显示

   -  【采购中心】自营批次列表详情锁定/释放记录 推送失败时增加失败原因

   -  【采购中心】批次列表 增加采购经理搜索

   -  【库存中心】库存分配-已分配渠道-有锁定库存-增加划转给指定渠道

   -  【库存中心】自营销售出库记录 推送失败时增加失败原因

   -  【库存中心】库存统计-sku详情的批次库存页增加_销售端占用的销售库存

   -  【订单中心】批次的销售库存由0变为大于0时，更新批次的销售状态为可售卖状态

   -  【售后订单】售后原因分类更新

   -  [入第三方平台仓]【渠道管理】增加标记及并联供应商

   -  [入第三方平台仓]【采购中心】自营批次-供货 并联了渠道的供应商

   -  [入第三方平台仓]【采购中心】增加供应商/仓库列表及增加供应商/渠道关系列表

   -  [对接获取/处理第三方渠道售后订单]【API拉单】-售后订单拉取

   -  [对接获取/处理第三方渠道售后订单]【API拉单】-渠道售后记录页面(包括小红书及拼多多)

   -  [对接获取/处理第三方渠道售后订单]【API拉单】-渠道售后操作（同意/拒绝）

   -  [进销存]【沙箱平台】--新增出库通知接口（仓库→行云）

   -  [进销存]BB销售出库批次扣减逻辑优化(对接了出库接口的以出库成功为节点提交至BB扣减库存)

-  2021.04.01 V5.4上线

   -  [分离货仓]【库存分配】分配给货仓的批次必须锁库

   -  [分离货仓]【渠道工作台】渠道工作台-自营将自营分类、自营品牌、自营标签从中台移除

   -  [分离货仓]【工单中心】工单中心-将客户充值工单和客户售后工单从中台移除

   -  [分离货仓]【控制面板】控制面板-订单变量中的待支付订单限时支付移除

   -  [分离货仓]【API管理】API管理-客户api中平台会员类型的数据隐藏移除

   -  [分离货仓]【内容中心】内容中心-除中台版本外的菜单功能从中台移除

   -  [分离货仓]【通知管理】通知管理-模板设置将平台会员模板从中台移除

   -  [分离货仓]【通知管理】通知管理-消息推送-将消息推送从中台移除

   -  [分离货仓]【通知管理】通知管理-站内通知将对象平台会员用户从中台移除

   -  [分离货仓]【订单中心】订单中心-api拉单-PI导单失败记录 中
      自营渠道类型的数据隐藏移除

   -  [分离货仓]【订单中心】订单中心-将支付订单从中台移除

   ps：如需使用被分离出的功能请至行云货仓后台进行操作

-  2021.03.12 V5.3上线

   -  【采购中心】批次列表-批次详情中_自营批次增加bb系统批次号显示

   -  【采购中心】自营批次 列表中增加 BBC锁库库存剩余 数据列

   -  【库存中心】自营库存-销售出库列表 增加推送状态筛选项

   -  【订单中心】渠道订单-待生成状态时 增加关闭操作

   -  【订单中心】生成失败的失败原因在记录时增加失败类型ID的标记

   -  【订单中心】发货单列表_增加以采购单号搜索的选项

   -  【订单中心】订单列表_下单账号优化为精确搜索

   -  【API拉单】取消渠道订单：取消原因增加：清关失败；海淘身份信息不一致

   -  【API拉单】已关闭/已取消的渠道订单触发通知下游取消

   -  【沙箱平台】API通知上游取消采购单

   -  【渠道工作台】导出 商品+批次数据的导出更新为异步导出
      (支持大数量数据导出)

   -  [源自自营货仓的订单]代发结算明细的销售税费须包括运费税费(修复历史数据)

-  2021.02.26 V5.2上线

   -  [渠道商品入第三方平台仓库,如pop]【采购中心】批次列表-批次列表的批次详情及批次采购的审核增加批次标记
      入第三方平台仓

   -  [渠道商品入第三方平台仓库,如pop]【渠道管理】添加渠道及渠道详情增加渠道标记
      入第三方平台仓

   -  [对接bb批次]【BBC销售出库单对接】销售出库及补单出库需要传“采购含税单价”、“中台订单号”、“条码ID”给BB系统

   -  [中台提醒]【通知管理】中台模板

   -  [中台提醒]
      [第三方渠道取消订单]【渠道工作台】-渠道设定-增加提醒设定

   -  【渠道工作台】商品总库-增加创建时间筛选、有无批次筛选、导出、批次

   -  【订单中心】采购订单-api推送记录-中渠道来源支持查找选择

   -  【订单中心】渠道订单-api拉单-api拉单渠道取消记录页面列表中
      订单渠道字段取值优化

   -  【订单中心】售后订单列表-增加订单渠道字段并增加筛选

-  2021.02.02 V5.1上线

   -  [对接BB批次]【采购中心】批次采购-中审核为已拒绝时，并联了BB批次的BBC批次请求释放BB锁定库存

   -  [对接BB批次]取消销售订单时如对应批次为不可售并有可调整出库的库存则自动调整出库并释放BB锁定库存

   -  [库存流程优化]发货后的退款退货成功完成，无物流单号即没有扣减\ *bb批次bbc锁定库存，释放*\ bb批次bbc锁定库存

   -  [库存流程优化]重推生成采购单，恢复原批次\ *批次共享可售库存、恢复原批次*\ 批次渠道可售库存、原批次-批次渠道已售库存

   -  [库存流程优化]发货前的仅退款成功完成后，恢复\ *批次共享可售库存以及恢复*\ 批次渠道可售库存

-  2021.01.15 V5.0上线

   -  【采购中心】-代发结算单详情-付款明细-增加付款凭证查看

   -  [批次增加部门字段]采购中心】-自营批次-详情-增加供货-增加
      并联所属部门字段

   -  【库存中心】-库存分配-全部渠道时也需要支持渠道的锁定库存

   -  【渠道工作台】报价和扣点导出的功能

   -  【订单中心】待重推列表-支持批量重推

   -  [渠道取消订单]【订单中心】-API拉单-API拉单-渠道取消记录

-  2020.12.29 V4.9上线

   -  【采购中心】自营批次-详情-锁定/释放记录

   -  【采购中心】采购中心-批次列表和批次采购的自营类型的批次详情增加库存主体字段

   -  【库存中心】销售出库-自营批次的实物出库记录

   -  【库存中心】库存调整-调整入库-自营类型的批次增加剩余可入库的数量判断

   -  【订单中心】渠道订单-api拉单和模板导单-详情-增加渠道订单轨迹

   -  【订单中心】API导单/模板导单-中，支持失败原因搜索

   -  【订单中心】订单列表- 已取消标签和渠道订单号查找

   -  【渠道工作台】渠道设定-支持按渠道自定义销售订单的确认/推送生效时间和时效

   -  【渠道工作台】渠道销售商品列表-增加批次/库存排序 @渠道

   -  【渠道工作台】ka销售-列表-报价页交互分为单价/规格总价 @渠道

   -  【渠道工作台】渠道工作台-显示当前渠道信用额度可用额度

   -  【控制面板】基础资料-公司主体-数据初始化

   -  【控制面板】基础资料-公司主体-数据初始化-供应商主体及适配

   -  【控制面板】基础资料-公司主体-数据初始化-渠道主体及适配

   -  【控制面板】基础资料-公司主体-数据初始化-采购结算主体及适配

   -  【控制面板】基础资料-公司主体-数据初始化-信用结算主体及适配

   -  【供应商平台】采购订单列表的导出中增加 运费模板名称

   -  【供应商平台】商品管理-供货管理-自营供应商隐藏供货操作入口

   -  【供应商平台】商品管理-入库商品列表-自营供应商隐藏供货操作入口

   -  【供应商平台】商品管理-商品入库-自营供应商禁用商品入库

   -  [售后订单对接智齿工单]

   -  [其他优化]采购单中运费税费的问题

   -  [其他优化]线下单的清关单号取值使用采购订单的单号即CG单号

-  2020.12.09 V4.8.1上线

   -  [新增功能] 渠道新增归属部门字段，库存分配时可以按照部门选择分配

   -  【订单中心】采购订单-API推送记录-推送成功的增加强制重推功能

   -  【订单中心】待重推订单-兼容线下单和已取消状态的订单不能重推

   -  【订单中心】发货单列表-增加物流单号显示

   -  【订单中心】订单列表-订单详情中的售后记录中显示信息

   -  【财务中心】供应商调整工单-增加批量审核功能及当前结果总金额

   -  【财务中心】采购中心/财务中心-代发结算单详情增加采购单的发货时间、支付方式、手续费

   -  【供应商平台】入库商品列表-增加税率

   -  【供应商平台】供货管理-导出增加采购价价格类型字段

   -  【商品中心】更改商品信息同步供应商的入库商品信息

   -  【采购中心】-批次列表-详情-增加供应商该商品的重量显示

   -  【供应商平台】-商品入库增加重量范围的检验

   -  【账户中心】发起信用账户结算，上传对账单，支持对账单包含订单条数增加到5W条，支持上传文件大小增加到6M

   -  【财务中心】发起信用账户结算，支持对当前筛选结果批量生产调整工单

   -  【采购中心】-批次列表和批次采购的自营类型的批次详情增加批次标记字段（如是否供金）

   -  【供应商平台】联营供应商且非保税订单过滤值(电商平台名称和编码)

-  2020.12.07 V4.8上线

   -  【商品中心】增加 仓库货号映射模块

   -  【商品中心】条码库数据来源于SKU

   -  【商品中心】增加 组合商品

   -  【渠道工作台】增加 总库及销售商品列表中增加对组合商品的支持

   -  【开放平台】接口兼容组合商品

-  2020.11.20 V4.7.2上线

   -  【商品中心】

      -  商品中心
         新建sku/编辑sku记录添加/更新内容、创建时间、创建人、更新时间、更新人
         ＠商品

   -  【订单中心】

      -  渠道订单-模板导单-线下单模板增加平台销售价类型 保税or 不保税

   -  【账户中心】

      -  供应商收支明细-导出优化 @财务

      -  信用账户-发起信用账户结算-取消订单的优化处理 @财务

      -  发起信用账户结算-当前结果的合值 @财务

      -  信用账户-列表中增加签约主体 @财务

   -  【财务中心】

      -  审核管理-信用账户调整工单-当前结果的合值 @财务

      -  应收管理-收款记录-详情中凭证可查看大图 ＠财务

   -  【供应商平台】

      -  采购订单列表-按发货单模板导出的字段顺序

-  2020.10.22 V4.7上线

   -  

      -  【开放平台】

         -  新增 通过渠道单号进行物流查询

      -  【商品中心】

         -  优化 sku列表-指定开售交互

      -  【渠道管理】

         -  优化 新增合同oa页面的收款单位支持修改

      -  【渠道工作台】【自主报价模式下】

         -  优化 ka商品销售列表优化

         -  优化 ka商品销售列表-报价页面优化

         -  优化 ka商品销售列表-效期页面优化（将效期从报价单中移出）

         -  优化 ka商品销售列表-报价导入支持日常的普通报价

      -  【库存中心】

         -  优化 库存分配-分配的搜索交互优化

      -  【账户中心】

         -  优化 供应商收支明细导出中增加供应商简称字段

         -  优化 发起信用账户结算-详情-明细增加取消订单的类型
            、支持减值、优化合计金额

         -  优化
            发起信用账户结算-详情-批量生成调整单-按类型金额生成调整工单

      -  【财务中心】

         -  优化 审核管理-供应商调整工单-增加申请人

         -  优化 审核管理-供应商调整工单-详情-增加申请人

         -  优化
            审核管理-售后订单-详情-显示财务审核备注和逾期自动拒绝记录

         -  优化 审核管理-信用账户调整工单-导出和汇总值

         -  优化 应收管理-增加收款-增加汇率等字段

      -  【供应商平台】

         -  优化 自营供应商的供货减值字段隐藏

         -  优化 发货单列表-海外直邮支持两段物流

         -  优化 采购订单列表支持当前结果导出

      另包括内容

      -  【库存中心】

         -  新增 BBC销售出库台账模块

      -  【采购中心】

         -  新增 “自营批次”模块-查看供货

         -  新增 “自营批次”模块-供货

         -  新增 “自营批次”模块-BBC供货列表

         -  新增 “自营批次”模块-查看批次信息

      -  【商品中心】

         -  新增 条码库模块

         -  优化 SKU列表编辑时字段调整

         -  优化 SPU列表新增商品功能调整

         -  优化 SPU列表字段调整

         -  优化 商品品牌字段调整

-  2020.10.03 V4.6.1上线

   -  对接智齿客服工单系统

-  2020.09.28 V4.6上线

   -  【供应商平台】供货批次-起发量默认为1可修改、自营仓库入库批号必填；采购订单列表-导入发货单优化

   -  【订单中心】订单生成失败-区编码不存在校验修改成
      区编码非空校验即可；采购订单-API推送记录；API订单-订单详情-信息编辑必填/选填；订单列表-增加销售毛利字段、及存储减值后成本；订单列表-发货单列表
      采购单列表-查看物流信息；订单列表-录入不同渠道店铺要求发货时效，并在订单中心做倒计时；订单列表-增加供应商查询字段；API拉单-API导单失败记录
      -订单生成成功后对应的失败记录不显示

   -  【渠道工作台】商品销售列表-销售在工作台中将他能卖的批次及价格按一定的格式导出来；商品销售列表-增加渠道可以自定义分配批次是否自动上架，联营、自营分开设置；商品销售列表-上架批次分自营/联营；可收缩左侧菜单；

   -  【渠道管理】渠道列表-被关闭的渠道自动释放所有库存

   -  【库存中心】出库规则-销售规则排序中供货价最低以减值后供货价为主；库存分配-详情-全部渠道；库存统计-详情中批次库存按批次状态显示批次状态

   -  【商品中心】SKU列表-SKU查看详情中-SPU编码可以支持复制

   -  【财务中心/供应商后台】付款单列表-付款核销金额字段取值变更

   -  行云全球汇PC/App -logo旁边需要加：行云全球汇旗下

   -  BBC结算单对接ERP优化需求--报错提示语

   -  【bug修复】-供应商平台付款方式为null修复

历史更新记录

-  2020.09.23 V4.5上线

-  2020.09.12 V4.3上线

-  2020.09.04 V4.2.2上线

-  2020.08.21 V4.2.1上线

-  2020.08.10 中台上线

-  2020.04.20 V2.7上线（渠道管理）

-  2020.01.15 V2.2上线（2.2 上线 站内信、app消息推送、文章(BBC)）

[STRIKEOUT:文档更新记录]

-  [STRIKEOUT:2020.12.08 更新操作手册]

-  [STRIKEOUT:2020.10.15 更新操作手册]

-  [STRIKEOUT:2020.09.29 更新增加操作手册]

-  [STRIKEOUT:2020.09.22 更新增加V4.0内容]

-  [STRIKEOUT:2019.10.25 初始]
