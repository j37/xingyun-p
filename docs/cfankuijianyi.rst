反馈/建议/产品阶段的需求及其他
==============================

-  线上提交反馈

   -  **tapd<需内部权限>**:https://www.tapd.cn/69695697/board/index?board_id=1169695697001000058#!order=default

   -  问卷网:https://www.wenjuan.com/s/InEFNn/

   -  腾讯文档:https://docs.qq.com/form/fill/DWnpUek54ZmZ2bU5M

-  线下TP会反馈建议

   -  [STRIKEOUT:每周四下午16:00-17:00]

-  联系作者

   -  SQ，产品经理

--------------

-  需求反馈状态包括产品阶段的需求（即预计下一版本要开发的内容）

   -  | 【腾讯文档】反馈需求列表
      | https://docs.qq.com/sheet/DWlpFTHFGQkdIZlRs

-  产品功能列表

   -  | 【腾讯文档】产品功能列表
      | https://docs.qq.com/sheet/DWlJPcXhla3lnbURV

-  其他说明

   -  | 【腾讯文档】产品说明
      | https://docs.qq.com/doc/DWkF3VFJFampmUVhK

--------------

-  产品开发团队

   -  LJX、HXC、SQ、YWP

   -  \|

   -  ZXQ、、LSD

   -  LCM、ZSY 、ZSL、HY、FXJ、JH、LX、、SKC

   -  ZY、FXD、JZY、CJJ、、WXF

   -  \|

   -  YDJ、XL、、ZP
