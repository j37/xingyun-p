.. _xingyun文档更新时间20210510:

xingyun文档(更新时间：2021.05.10)
=================================

   更新时间：2021.03.23

   常驻杭州，工位在15楼行政区域

   正在进行中的产品原型：

-  `更新记录 <http://xingyun.thesql.net/en/latest/agengxin.html>`__

-  `更新预告 <http://xingyun.thesql.net/en/latest/byugao.html>`__

-  `反馈/建议、产品阶段的需求、及其他 <http://xingyun.thesql.net/en/latest/cfankuijianyi.html>`__

-  `中台系统操作说明文档 <http://xingyun.thesql.net/en/latest/drumenzhinan.html>`__

-  `中台系统常见问题 <http://xingyun.thesql.net/en/latest/echangjianwenti.html>`__

-  `行云业务中台操作手册 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html>`__

   -  `概述 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n3>`__

   -  `一、控制面板 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n40>`__

   -  `二、商品中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n96>`__

   -  `三、供应商管理 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n136>`__

   -  `四、采购中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n417>`__

   -  `五、库存中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n176>`__

   -  `六、价格中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n217>`__

   -  `七、渠道管理 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n424>`__

   -  `八、渠道工作台 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n236>`__

   -  `九、订单中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n265>`__

   -  `十、工单中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n322>`__

   -  `十一、账户中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n337>`__

   -  `十二、财务中心 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n367>`__

   -  `十三、API管理 <http://xingyun.thesql.net/en/latest/fcaozuoshouce.html#header-n408>`__

..

   简约、朴素、自然、快速迭代可扩展

   作为产品界的小学生，可能格局比较低、视野比较窄，思考比较浅。但是我相信学习以及相信相信的力量。以此共勉

.. figure:: http://ia.51.la/go1?id=21051615&pvFlag=1
   :alt: 
